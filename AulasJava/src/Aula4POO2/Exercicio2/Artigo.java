package Aula4POO2.Exercicio2;

import java.util.List;

public class Artigo {

	private String titulo;
	private String texto;
	private List<String> autores;

	public Artigo(String titulo, String texto, List<String> autores) {
		this.titulo = titulo;
		this.texto = texto;
		this.autores = autores;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public List<String> getAutores() {
		return autores;
	}

	public void setAutores(List<String> autores) {
		this.autores = autores;
	}

	public void exibirInformacoes() {
		System.out.println("Artigo: " + titulo);
		System.out.println("Autores: " + autores);
		System.out.println("Texto: " + texto);
	}
}
