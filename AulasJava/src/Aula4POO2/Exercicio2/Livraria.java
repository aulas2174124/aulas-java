package Aula4POO2.Exercicio2;

import java.util.ArrayList;
import java.util.List;

//Dada uma livraria que comercializa livros, revistas e jornais.
//Os livros possuem titulo, um ou mais autores, editora, data de publicação e o preço da venda; 
//A revista possui editora, numero da edição, data de publicação, preço de venda e os artigos que por sua vez, possuem titulo, o texto da publicação e autores. 
//E os jornais possuem valor de venda, titulo, data de publicação e as reportagens, que possuem titulo, um ou mais autores, texto da reportagem; 
//Implemente o gerenciamento dos itens da livraria:
//Inclusão de um novo item;
//Alteração de informações de um item já existente;
//Listagem dos itens da livraria: código + tipo de item + título;
//Listagem dos itens da livraria para à venda: Todas as informações de cada um dos itens;
//Modalidades de venda: Estudante: possui 50% de desconto na compra de um livro ou revista, mas paga o valor integral para os jornais enquanto os demais clientes: pagam o valor integral dos itens;

public class Livraria {
	private List<Livro> livros;
	private List<Revista> revistas;
	private List<Jornal> jornais;

	public List<Livro> getLivros() {
		return livros;
	}

	public void setLivros(List<Livro> livros) {
		this.livros = livros;
	}

	public List<Revista> getRevistas() {
		return revistas;
	}

	public void setRevistas(List<Revista> revistas) {
		this.revistas = revistas;
	}

	public List<Jornal> getJornais() {
		return jornais;
	}

	public void setJornais(List<Jornal> jornais) {
		this.jornais = jornais;
	}

	public Livraria() {
		livros = new ArrayList<>();
		revistas = new ArrayList<>();
		jornais = new ArrayList<>();
	}

	public void incluirLivro(Livro livro) {
		livros.add(livro);
	}

	public void incluirRevista(Revista revista) {
		revistas.add(revista);
	}

	public void incluirJornal(Jornal jornal) {
		jornais.add(jornal);
	}

	public void editarLivro(int indice, Livro livro) {
		if (indice >= 0 && indice < livros.size()) {
			livros.set(indice, livro);
			System.out.println("Livro editado com sucesso!");
		} else {
			System.out.println("Erro ao editar o Livro.");
		}
	}

	public void editarRevista(int indice, Revista revista) {
		if (indice >= 0 && indice < revistas.size()) {
			revistas.set(indice, revista);
			System.out.println("Revista editada com sucesso!");
		} else {
			System.out.println("Erro ao Editar a Revista.");
		}
	}

	public void editarJornal(int indice, Jornal jornal) {
		if (indice >= 0 && indice < jornais.size()) {
			jornais.set(indice, jornal);
			System.out.println("Jornal editado com sucesso!");
		} else {
			System.out.println("Erro ao Edittar o Jornal.");
		}
	}

	public void listarItens() {
		System.out.println("Livros:");
		for (Livro livro : livros) {
			System.out.println("Título: " + livro.getTitulo());
		}

		System.out.println("Revistas:");
		for (Revista revista : revistas) {
			System.out.println("Editora: " + revista.getEditora());
		}

		System.out.println("Jornais:");
		for (Jornal jornal : jornais) {
			System.out.println("Título: " + jornal.getTitulo());
		}
	}

	public void listarItensAVenda() {
		System.out.println("Livros à venda:");
		for (Livro livro : livros) {
			System.out.println("Título: " + livro.getTitulo());
			System.out.println("Autores: " + livro.getAutores());
			System.out.println("Editora: " + livro.getEditora());
			System.out.println("Data de Publicação: " + livro.getDataPublicada());
			System.out.println("Preço de Venda: " + livro.getPreco());
			System.out.println("----------------------------------------");
		}

		System.out.println("Revistas à venda:");
		for (Revista revista : revistas) {
			System.out.println("Editora: " + revista.getEditora());
			System.out.println("Número da Edição: " + revista.getNumeroEdicao());
			System.out.println("Data de Publicação: " + revista.getDataPublicacao());
			System.out.println("Preço de Venda: " + revista.getPrecoVenda());
			System.out.println("Artigos:");
			for (Artigo artigo : revista.getArtigos()) {
				System.out.println("Título: " + artigo.getTitulo());
				System.out.println("Texto: " + artigo.getTexto());
				System.out.println("Autores: " + artigo.getAutores());
				System.out.println("----------------------------------------");
			}
		}
			System.out.println("Jornais à venda:");
			for (Jornal jornal : jornais) {
				System.out.println("Título: " + jornal.getTitulo());
				System.out.println("Data de Publicação: " + jornal.getDataPublicada());
				System.out.println("Preço de Venda: " + jornal.getValorVenda());
				System.out.println("Reportagens:");
				for (Reportagem reportagem : jornal.getReportagens()) {
					System.out.println("Título: " + reportagem.getTitulo());
					System.out.println("Texto: " + reportagem.getTexto());
					System.out.println("Autores: " + reportagem.getAutores());
					System.out.println("----------------------------------------");
				}
			}
		}
	

	public void listarModalidadesVenda() {
		System.out.println("----------------------------------------");
		System.out.println("Modalidades de Venda:");
		System.out.println("1. Estudante: 50% de desconto em livros e revistas.");
		System.out.println("2. Outros clientes: valor integral para todos os itens.");
	}
	
	

}
