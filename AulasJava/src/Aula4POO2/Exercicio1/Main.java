package Aula4POO2.Exercicio1;

public class Main {
	public static void main(String[] args) {
		Elevador elevador = new Elevador(3, 5);

		elevador.entrar();
		elevador.subir();
		//elevador.entrar();
		elevador.subir();
		elevador.sair();
		elevador.descer();
		elevador.descer();
		elevador.descer();
		elevador.entrar();
		elevador.subir();
		elevador.subir();
		elevador.subir();
		elevador.subir();
		elevador.subir();
		elevador.subir();
		elevador.entrar();
		elevador.entrar();
		elevador.entrar();
		elevador.descer();

		System.out.println("------------------------------------------------------");
		System.out.println("O elevador esta no Andar: " + elevador.getAndarAtual());
		System.out.println("O prédio tem tantos andares: " + elevador.getTotalAndares());
		System.out.println("O elevador tem a capacidade de: " + elevador.getCapacidade());
		System.out.println("O elevador Contém: " + elevador.getPessoasPresentes() + " pessoas atualmente!");
	}
}