package Aula2;

//2) Dados dois números reais, imprima se são iguais e, caso sejam diferentes, imprima qual é maior;

public class Exercicio2 {
	public static void exercicio2(double num1, double num2){
			 boolean iguais = true;
			 
			 if (num1 != num2) {
				 iguais = false;
			 }
			 
			 if (iguais) {
				 System.out.println("Números são iguais. ");
			 } else {
				 System.out.println("Números são diferentes.");
				
				 double numeroMaior = num1 > num2 ? num1 : num2;
				 double numeroMenor = num1 < num2 ? num1 : num2;
				 
				 System.out.println("Primeiro número (" + num1 + ") é " + ( num1 > num2 ?  "maior" : "menor" ) + "que o segundo número (" + num2 + "). ");
				 System.out.println("O maior número é: " + numeroMaior);
				 System.out.println("O menor número é: " + numeroMenor);
			 }
		}
		
		public static void main(String[] args) { 
			
			double numero1 = 3.10;
			double numero2 = 2.70;
			
			exercicio2(numero1, numero2);
		}
	}

