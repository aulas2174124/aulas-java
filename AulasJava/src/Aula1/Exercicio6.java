package Aula1;

//6) Múltiplos de 10: Dado array, você deve avaliar cada posição. Quando encontrar um múltiplo de 10, você 
//deve substituir os próximos valores por esse múltiplo. Caso encontre outro, 
//o valor deve mudar para este novo múltiplo. Ex: 1, 10, 11, 20, 12 -> 1, 10, 10, 20, 20.

public class Exercicio6 {
	public static void exercicio6(int[] array) {
        int multiplo = -1;

        for (int i = 0; i < array.length; i++) {
            if (array[i] % 10 == 0) {
                multiplo = array[i];
            } else if (multiplo != -1) {
                array[i] = multiplo;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = {1, 10, 11, 20, 12};

        exercicio6(array);

        System.out.println("Múltiplos de 10:");
        for (int num : array) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
