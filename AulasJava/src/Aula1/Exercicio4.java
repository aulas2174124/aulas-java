package Aula1;

//4) Primos: Dado um número, verifique e retorne se o número informado é primo.

public class Exercicio4 {

	 public static boolean Primo(int numero) {
	        if (numero <= 1) {
	            return false;
	        }

	        for (int i = 2; i <= Math.sqrt(numero); i++) {
	            if (numero % i == 0) {
	                return false;
	            }
	        }

	        return true;
	    }

	    public static void main(String[] args) {
	        int numero = 19;

	        if (Primo(numero)) {
	            System.out.println(numero + " é um número primo.");
	        } else {
	            System.out.println(numero + " não é número primo.");
	        }
	    }
	}
	
	

