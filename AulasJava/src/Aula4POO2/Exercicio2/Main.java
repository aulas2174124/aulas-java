package Aula4POO2.Exercicio2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		Livraria livraria = new Livraria();
		Scanner scanner = new Scanner(System.in);

		int opcao;

		do {
			exibirMenu();
			opcao = scanner.nextInt();
			scanner.nextLine();

			switch (opcao) {
				case 1:
					adicionarLivro(livraria, scanner);
					break;
				case 2:
					adicionarRevista(livraria, scanner);
					break;
				case 3:
					adicionarJornal(livraria, scanner);
					break;
				case 4:
					listarItens(livraria, scanner);
					break;
				case 5:
					editarItem(livraria, scanner);
					break;
				case 0:
					System.out.println("Saindo do programa...");
					break;
				default:
					System.out.println("Opção inválida. Por favor, tente novamente.");
					break;
			}

			System.out.println();
		} while (opcao != 0);

		scanner.close();
	}

	private static void exibirMenu() {
		System.out.println("Menu:");
		System.out.println("1. Adicionar Livro");
		System.out.println("2. Adicionar Revista");
		System.out.println("3. Adicionar Jornal");
		System.out.println("4. Listar Itens para à venda");
		System.out.println("5. Editar Item");
		System.out.println("0. Sair");
		System.out.print("Digite a opção desejada: ");
		System.out.println("");
		System.out.println("----------------------------------------");
		System.out.println("");
	}

	private static void adicionarLivro(Livraria livraria, Scanner scanner) {
		System.out.println("Adicionar Livro");

		System.out.print("Título do livro: ");
		String titulo = scanner.nextLine();

		System.out.print("Autores do livro (separados por vírgula): ");
		String autoresInput = scanner.nextLine();
		List<String> autores = Arrays.asList(autoresInput.split(","));

		System.out.print("Editora do livro: ");
		String editora = scanner.nextLine();

		System.out.print("Data de publicação do livro: ");
		String dataPublicacao = scanner.nextLine();

		System.out.print("Preço de venda do livro: ");
		String precoVendaStr = scanner.nextLine();
		double precoVenda = Double.parseDouble(precoVendaStr);

		Livro livro = new Livro(titulo, autores, editora, dataPublicacao, precoVenda);
		livraria.incluirLivro(livro);

		System.out.println("Livro adicionado com sucesso!");
	}

	private static void adicionarRevista(Livraria livraria, Scanner scanner) {
		System.out.println("Adicionar Revista");

		System.out.print("Título da revista: ");
		String titulo = scanner.nextLine();

		System.out.print("Editora da revista: ");
		String editora = scanner.nextLine();

		System.out.print("Data de publicação da revista: ");
		String dataPublicacao = scanner.nextLine();

		System.out.print("Preço de venda da revista: ");
		String precoVendaStr = scanner.nextLine();
		double precoVenda = Double.parseDouble(precoVendaStr);

		Revista revista = new Revista(titulo, editora, dataPublicacao, precoVenda);

		boolean adicionarArtigo = true;
		while (adicionarArtigo) {
			System.out.print("Título do artigo: ");
			String tituloArtigo = scanner.nextLine();

			System.out.print("Texto do artigo: ");
			String textoArtigo = scanner.nextLine();

			System.out.print("Autores do artigo (separados por vírgula): ");
			String autoresInput = scanner.nextLine();
			List<String> autores = Arrays.asList(autoresInput.split(","));

			Artigo artigo = new Artigo(tituloArtigo, textoArtigo, autores);
			revista.adicionarArtigo(artigo);

			System.out.print("Deseja adicionar outro artigo? (S/N): ");
			String resposta = scanner.nextLine();
			adicionarArtigo = resposta.equalsIgnoreCase("S");
		}

		livraria.incluirRevista(revista);

		System.out.println("Revista adicionada com sucesso!");
	}

	private static void adicionarJornal(Livraria livraria, Scanner scanner) {
		System.out.println("Adicionar Jornal");

		System.out.print("Título do jornal: ");
		String titulo = scanner.nextLine();

		System.out.print("Data de publicação do jornal: ");
		String dataPublicacao = scanner.nextLine();

		System.out.print("Preço de venda do jornal: ");
		String valorVendaStr = scanner.nextLine();
		double valorVenda = Double.parseDouble(valorVendaStr);

		Jornal jornal = new Jornal(valorVenda, dataPublicacao, titulo);

		boolean adicionarReportagem = true;
		while (adicionarReportagem) {
			System.out.print("Título da reportagem: ");
			String tituloReportagem = scanner.nextLine();

			System.out.print("Texto da reportagem: ");
			String textoReportagem = scanner.nextLine();

			System.out.print("Autores da reportagem (separados por vírgula): ");
			String autoresInput = scanner.nextLine();
			List<String> autores = Arrays.asList(autoresInput.split(","));

			Reportagem reportagem = new Reportagem(tituloReportagem, textoReportagem, autores);
			jornal.adicionarReportagem(reportagem);

			System.out.print("Deseja adicionar outra reportagem? (S/N): ");
			String resposta = scanner.nextLine();
			adicionarReportagem = resposta.equalsIgnoreCase("S");
		}

		livraria.incluirJornal(jornal);

		System.out.println("Jornal adicionado com sucesso!");
	}

	private static void listarItens(Livraria livraria, Scanner scanner) {
		System.out.println("Listagem de Itens da Livraria");
		livraria.listarItensAVenda();
		livraria.listarModalidadesVenda();

	}

	private static void editarItem(Livraria livraria, Scanner scanner) {
		System.out.println("Menu de Edição");
		System.out.println("Escolha o tipo de item a ser editado:");
		System.out.println("1. Livro");
		System.out.println("2. Revista");
		System.out.println("3. Jornal");

		System.out.print("Opção: ");
		int opcao = scanner.nextInt();
		scanner.nextLine();

		switch (opcao) {
			case 1:
				editarLivro(livraria, scanner);
				break;
			case 2:
				editarRevista(livraria, scanner);
				break;
			case 3:
				editarJornal(livraria, scanner);
				break;
			default:
				System.out.println("Opção inválida.");
				break;

		}

	}

	private static void editarLivro(Livraria livraria, Scanner scanner) {
		System.out.println("Editar Livro");

		System.out.print("Informe o número do Livro que deseja Editar: ");
		int indice = scanner.nextInt();
		scanner.nextLine();

		if (indice >= 0 && indice < livraria.getLivros().size()) {
			Livro livro = livraria.getLivros().get(indice);

			System.out.println("Informações atuais do Livro:");
			System.out.println("Editora: " + livro.getEditora());
			System.out.println("Data de Publicação: " + livro.getDataPublicada());
			System.out.println("Preço de Venda: " + livro.getPreco());

			System.out.print("Novo título: ");
			String novoTitulo = scanner.nextLine();
			livro.setTitulo(novoTitulo);

			System.out.print("Novos autores (separados por vírgula): ");
			String autores = scanner.nextLine();
			List<String> novosAutores = Arrays.asList(autores.split(","));
			livro.setAutores(novosAutores);

			System.out.print("Nova editora: ");
			String novaEditora = scanner.nextLine();
			livro.setEditora(novaEditora);

			System.out.print("Nova data de publicação: ");
			String novaData = scanner.nextLine();
			livro.setDataPublicada(novaData);

			System.out.print("Novo preço de venda: ");
			double novoPreco = scanner.nextDouble();
			livro.setPreco(novoPreco);

			System.out.println("Livro editado com sucesso!");
		} else {
			System.out.println("Número Invalida, Digite novamente! ");
		}
	}

	private static void editarRevista(Livraria livraria, Scanner scanner) {
		System.out.println("Informe o número da revista que deseja Editar:");
		int indice = scanner.nextInt();
		scanner.nextLine();
		List<Revista> revistas = livraria.getRevistas();
		if (indice >= 0 && indice < revistas.size()) {
			Revista revista = revistas.get(indice);

			System.out.println("Informações atuais da revista:");
			System.out.println("Editora: " + revista.getEditora());
			System.out.println("Data de Publicação: " + revista.getDataPublicacao());
			System.out.println("Preço de Venda: " + revista.getPrecoVenda());

			System.out.println("Digite as novas informações da revista:");

			System.out.print("Editora: ");
			String novaEditora = scanner.nextLine();

			System.out.print("Data de Publicação: ");
			String novaDataPublicacao = scanner.nextLine();

			System.out.print("Preço de Venda: ");
			double novoPrecoVenda = scanner.nextDouble();
			scanner.nextLine();

			revista.setEditora(novaEditora);
			revista.setDataPublicacao(novaDataPublicacao);
			revista.setPrecoVenda(novoPrecoVenda);

			System.out.println("Revista atualizada com sucesso!");
		} else {
			System.out.println("Número Invalido, Digite novamente! ");
		}
	}

	private static void editarJornal(Livraria livraria, Scanner scanner) {
	    System.out.println("Informe o número do Jornal que deseja Editar:");
	    int indice = scanner.nextInt();
	    scanner.nextLine();

	    List<Jornal> jornais = livraria.getJornais();
	    if (indice >= 0 && indice < jornais.size()) {
	        Jornal jornal = jornais.get(indice);

	        System.out.println("Informações atuais do jornal:");
	        System.out.println("Título: " + jornal.getTitulo());
	        System.out.println("Data de Publicação: " + jornal.getDataPublicada());
	        System.out.println("Valor de Venda: " + jornal.getValorVenda());

	        System.out.println("Digite as novas informações do jornal:");

	        System.out.print("Título: ");
	        String novoTitulo = scanner.nextLine();

	        System.out.print("Data de Publicação: ");
	        String novaDataPublicacao = scanner.nextLine();

	        double novoValorVenda = 0.0;
	        boolean valorVendaValido = false;
	        while (!valorVendaValido) {
	            System.out.print("Valor de Venda: ");
	            if (scanner.hasNextDouble()) {
	                novoValorVenda = scanner.nextDouble();
	                scanner.nextLine();
	                valorVendaValido = true;
	            } else {
	                System.out.println("Valor de Venda inválido. Digite um número válido.");
	                scanner.nextLine(); 
	            }
	        }

	        jornal.setTitulo(novoTitulo);
	        jornal.setDataPublicada(novaDataPublicacao);
	        jornal.setValorVenda(novoValorVenda);

	        System.out.println("Jornal atualizado com sucesso!");
	    } else {
	        System.out.println("Número Inválido, Digite novamente!");
	    }
	}
	}



//	    Livro livro1 = new Livro("Livro 1", Arrays.asList("Autor 1"), "Editora 1", "01/01/2023", 29.99);
//	    livraria.incluirLivro(livro1);
//
//	    Revista revista1 = new Revista("Revista 1", "Editora 1", "01/01/2023", 9.99);
//	    revista1.adicionarArtigo(new Artigo("Artigo 1", "Texto 1", Arrays.asList("Autor 1")));
//	    revista1.adicionarArtigo(new Artigo("Artigo 2", "Texto 2", Arrays.asList("Autor 2")));
//	    livraria.incluirRevista(revista1);
//
////	    Jornal jornal1 = new Jornal(5.99, "01/01/2023", "Jornal 1");
////	    jornal1.adicionarReportagem(new Reportagem("Reportagem 1", "Texto 1", Arrays.asList("Autor 1")));
////	    jornal1.adicionarReportagem(new Reportagem("Reportagem 2", "Texto 2", Arrays.asList("Autor 2")));
////	    livraria.incluirJornal(jornal1);
//
//	    livraria.listarItens();
//	    livraria.listarItensAVenda();
//	    livraria.listarModalidadesVenda();
