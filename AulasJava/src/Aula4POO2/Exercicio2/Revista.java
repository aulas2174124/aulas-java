package Aula4POO2.Exercicio2;

import java.util.ArrayList;
import java.util.List;

public class Revista {
	private String editora;
	private String numeroEdicao;
	private String dataPublicacao;
	private double precoVenda;
	private List<Artigo> artigos;

	public Revista(String editora, String string, String dataPublicacao, double precoVenda) {
		this.editora = editora;
		this.numeroEdicao = string;
		this.dataPublicacao = dataPublicacao;
		this.precoVenda = precoVenda;
		this.artigos = new ArrayList<>();
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public void setNumeroEdicao(String numeroEdicao) {
		this.numeroEdicao = numeroEdicao;
	}

	public void setDataPublicacao(String dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public void setPrecoVenda(double precoVenda) {
		this.precoVenda = precoVenda;
	}

	public void setArtigos(List<Artigo> artigos) {
		this.artigos = artigos;
	}

	public String getEditora() {
		return editora;
	}

	public String getNumeroEdicao() {
		return numeroEdicao;
	}

	public String getDataPublicacao() {
		return dataPublicacao;
	}

	public double getPrecoVenda() {
		return precoVenda;
	}

	public List<Artigo> getArtigos() {
		return artigos;
	}

	public void adicionarArtigo(Artigo artigo) {
		artigos.add(artigo);
	}

	public void exibirInformacoes() {
		System.out.println("Revista");
		System.out.println("Editora: " + editora);
		System.out.println("Número da Edição: " + numeroEdicao);
	}
}
