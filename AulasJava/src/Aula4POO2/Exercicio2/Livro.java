package Aula4POO2.Exercicio2;

import java.util.List;

public class Livro {
	private String titulo;
	private List<String> autores;
	private String editora;
	private String dataPublicada;
	private double preco;

	public Livro(String titulo, List<String> autores, String editora, String dataPublicacao, double precoVenda) {
		this.titulo = titulo;
		this.autores = autores;
		this.editora = editora;
		this.dataPublicada = dataPublicacao;
		this.preco = precoVenda;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<String> getAutores() {
		return autores;
	}

	public void setAutores(List<String> autores) {
		this.autores = autores;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public String getDataPublicada() {
		return dataPublicada;
	}

	public void setDataPublicada(String dataPublicada) {
		this.dataPublicada = dataPublicada;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public void exibirInformacoes() {
		System.out.println("Livro: " + titulo);
		System.out.println("Autores: " + autores);
		System.out.println("Editora: " + editora);
		System.out.println("Data de Publicação: " + dataPublicada);
		System.out.println("Preço de Venda: " + preco);
	}
}