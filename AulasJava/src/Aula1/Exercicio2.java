package Aula1;

import java.util.stream.IntStream;

//2) Soma: Dado determinado número, vocês devem efetuar a soma de 0 até o número informado conforme o nome do método. 
//Par: Apenas a soma de números pares. Impar: Apenas a soma de números ímpares.


public class Exercicio2 {
	

	
	    public static int somaTotal(int numero) {
	        return IntStream.rangeClosed(0, numero).sum();
	    }

	    public static int somaPares(int numero) {
	        return IntStream.rangeClosed(0, numero)
	                .filter(n -> n % 2 == 0)
	                .sum();
	    }

	    public static int somaImpares(int numero) {
	        return IntStream.rangeClosed(0, numero)
	                .filter(n -> n % 2 != 0)
	                .sum();
	    }

	    public static void main(String[] args) {
	        int numero = 20;

	        int somaTotal = somaTotal(numero);
	        int somaPar = somaPares(numero);
	        int somaImpar = somaImpares(numero);

	        System.out.println("Soma : " + somaTotal);
	        System.out.println("Soma dos números pares: " + somaPar);
	        System.out.println("Soma dos números ímpares: " + somaImpar);
	    }
	}

