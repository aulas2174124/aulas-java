package Aula1;

//3) Remoção de caracteres: Dado um texto, vocês devem remover as vogais dele e retornar o texto sem estes caracteres;

public class Exercicio3 {

	  public static String removerVogais(String texto) {
	        return texto.replaceAll("[aeiouAEIOU]", "");
	    }

	    public static void main(String[] args) {
	        String texto = "Imagine uma nova história para sua vida e acredite nela.";

	        String textoSemVogais = removerVogais(texto);

	        System.out.println("Retirando as Vogais: " + textoSemVogais);
	    }
	
}
