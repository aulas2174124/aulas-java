package Aula1;

//1) Extrato: Na empresa em que trabalhamos, 
//há tabelas com o gasto de cada mês. Para fechar o balanço 
//do primeiro trimestre, precisamos somar o gasto total. Sabendo 
//que, em janeiro, foram gastos 15 mil reais, em fevereiro, 23 mil 
//reais e, em março, 17 mil reais, faça uma classe que calcule e imprima
//a despesa total no trimestre e a média mensal de gastos.

public class Exercicio1 {
	private int janeiro;
    private int fevereiro;
    private int marco;

    public Exercicio1(int janeiro, int fevereiro, int marco) {
        this.janeiro = janeiro;
        this.fevereiro = fevereiro;
        this.marco = marco;
    }

    public int calcularDespesaTotal() {
        return janeiro + fevereiro + marco;
    }

    public double calcularMediaMensal() {
        return calcularDespesaTotal() / 3.0;
    }

    public static void main(String[] args) {
        int gastoJaneiro = 15000;
        int gastoFevereiro = 23000;
        int gastoMarco = 17000;

        Exercicio1 balanco = new Exercicio1(gastoJaneiro, gastoFevereiro, gastoMarco);

        int despesaTotal = balanco.calcularDespesaTotal();
        double mediaMensal = balanco.calcularMediaMensal();

        System.out.println("Despesa do trimestre R$: " + despesaTotal);
        System.out.println("Média mensal de gastos R$: " + mediaMensal);
    }
}

