package Aula4POO2.Exercicio3;

import java.util.List;

public class Pedido {
	private static int numeroPedidoAtual = 1;
	private int numero;
	private Cliente cliente;
	private List<Produto> itens;
	private String status;
	private String formaPagamento;
	private boolean retirada;
	private String enderecoEntrega;
	private double taxaEntrega;

	public Pedido(String cliente, List<String> itens, String status, String formaPagamento, boolean retirada,
			String enderecoEntrega, double taxaEntrega) {
		this.numero = numeroPedidoAtual++;

	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Produto> getItens() {
		return itens;
	}

	public void setItens(List<Produto> itens) {
		this.itens = itens;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(String formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public boolean isRetirada() {
		return retirada;
	}

	public void setRetirada(boolean retirada) {
		this.retirada = retirada;
	}

	public String getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public void setEnderecoEntrega(String enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}

	public double getTaxaEntrega() {
		return taxaEntrega;
	}

	public void setTaxaEntrega(double taxaEntrega) {
		this.taxaEntrega = taxaEntrega;
	}

	public void adicionarItem(Produto produto) {
	}

	public void atualizarStatus(String novoStatus) {
		status = novoStatus;
	}

	public void enviarMensagemCliente() {
		System.out.println("Mensagem enviada para o cliente:");
		System.out.println("Pedido número: " + numero);
		System.out.println("Status: " + status);
		System.out.println("----------------------------------------");
	}
}
