package Aula2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Collections;

//4) Dado a lista: [ "javascript", "typescript", "c++", "java", "rust", "ruby", "python", "lua", "go" ]
//Imprima:
//O tamanho dessa lista;
//Se a lista contém "java", imprima "java é o melhor" se não "Faltou o melhor" ;
//E crie uma nova lista com os nomes ordenados alfabeticamente

public class Exercicio4 {
	public static void main(String[] args) {
		List<String> lista = new ArrayList<>();
		lista.add("javascript");
        lista.add("typescript");
        lista.add("c++");
        lista.add("java");
        lista.add("rust");
        lista.add("ruby");
        lista.add("python");
        lista.add("lua");
        lista.add("go");
        System.out.println("Lista: " + lista.size());
        
        if (lista.contains("java")) {
        	System.out.println("Java é o melhor!");
        }else {
        	System.out.println("Faltou o melhor!");
        }
        
        List<String> Ordenalista = lista.stream()
        		.sorted()
        		.collect(Collectors.toList());
        		
        
        System.out.println("Lista ordenada: ");
       lista.forEach(System.out::println);
        
	}

}
