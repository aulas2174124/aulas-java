package Aula1;

//5) Apenas 1 e 4: Você deve fazer um método que avalia o array passado e diz se ele só contém 1 e 4 :)

public class Exercicio5 {

    public static boolean exercicio5(int[] array) {
        for (int num : array) {
            if (num != 1 && num != 4) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int[] array_1 = {1, 4, 1, 4};  
     

        if (exercicio5(array_1)) {
            System.out.println("O array 1 contém apenas 1 e 4.");
        } else {
            System.out.println("O array1 contém outros números além de 1 e 4.");
        }

    }
	
}
