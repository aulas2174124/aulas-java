package Aula2;

import java.util.Arrays;

//3) Dado um nome composto, imprima "Sim" se o nome tiver 10 ou mais caracteres e a palavra 'java' Ou "não" se não atender estes requisitos;

public class Exercicio3 {
	public static void exercicio3(String nome) {
		boolean exercicio3 = Arrays.stream(nome.split(" "))
				.anyMatch(palavra -> palavra.length() >= 10 && palavra.toLowerCase().contains("java"));
		
		if (exercicio3) {
			System.out.println("Sim!");
		} else {
			System.out.println("Não!");
		}
	}
	
	public static void main(String[] args) {
		String nome = "HTML é uma linguagem de programação?";
		
		exercicio3(nome);
	}

}
