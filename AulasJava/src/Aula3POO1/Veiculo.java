package Aula3POO1;

public class Veiculo {
	
	  private String cor;
	    private int quantidadeDeRodas;
	    private String carroceria;
	    private String chassi;

	    public Veiculo(String cor, int quantidadeDeRodas, String carroceria, String chassi) {
	        this.cor = cor;
	        this.quantidadeDeRodas = quantidadeDeRodas;
	        this.carroceria = carroceria;
	        this.chassi = chassi;
	    }

		public String getCor() {
			return cor;
		}

		public void setCor(String cor) {
			this.cor = cor;
		}

		public int getQuantidadeDeRodas() {
			return quantidadeDeRodas;
		}

		public void setQuantidadeDeRodas(int quantidadeDeRodas) {
			this.quantidadeDeRodas = quantidadeDeRodas;
		}

		public String getCarroceria() {
			return carroceria;
		}

		public void setCarroceria(String carroceria) {
			this.carroceria = carroceria;
		}

		public String getChassi() {
			return chassi;
		}

		public void setChassi(String chassi) {
			this.chassi = chassi;
		}

		public void exibirDetalhes() {
			System.out.println("Tipo de Veículo: " + getClass().getSimpleName());
	        System.out.println("Chassi do Veículo: " + chassi);
	        System.out.println("Carroceria:" + carroceria);
	        System.out.println("------------------------------------------------");
	    }
	
}
