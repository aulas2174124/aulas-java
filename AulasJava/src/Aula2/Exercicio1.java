package Aula2;

//1) Criar um método que dados dois numeros inteiros, imprima a soma, a subtração, a multiplicação, a divisão e o resto da divisão;

public class Exercicio1 {
	
	public static void exercicio(int a, int b) {
		
		int soma = a + b;
		int subtracao = a - b;
		int multipicacao = a * b;
		int divisao = a / b;
		int resto = a % b;
		
		System.out.println("Soma: " + soma);
        System.out.println("Subtração: " + subtracao);
        System.out.println("Multiplicação: " + multipicacao);
        System.out.println("Divisão: " + divisao);
        System.out.println("Resto da Divisão: " + resto);
		
		
	}
	public static void main(String[] args) {
		
		int num1 = 10;
		int num2 = 15;
		
		exercicio(num1, num2);
	}
}


