package Aula3POO1;

//Para praticar os conceitos de orientação a objetos, segue um exercicio:
//
//Implemente um sistema que gerencia a venda de veículos de uma concessrionária.
//- Todo veiculo deve ter cor, quantidadeDeRodas, carroceria, chassi;
//- Neste sistema podemos ter carros, motos e caminhoes.
//
//No final, instancie um veiculo de cada tipo e mostre no console uma mensagem que indique o chassi de cada veiculo e o tipo dele.

public class Exercicio {
	   public static void main(String[] args) {
	        Carro carro = new Carro("Branco", 4, "hatch", "168Arr03AX82A1791");
	        Moto moto = new Moto("Vermelho", 2, "Esportiva", "43tV4l89LA19D2441");
	        Caminhao caminhao = new Caminhao("Azul", 6, "Carga", "8CR6esKRkSAmt8540");

	        carro.exibirDetalhes();
	        moto.exibirDetalhes();
	        caminhao.exibirDetalhes();
	    }
	   
}
