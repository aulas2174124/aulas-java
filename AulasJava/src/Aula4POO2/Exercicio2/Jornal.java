package Aula4POO2.Exercicio2;

import java.util.ArrayList;
import java.util.List;

public class Jornal {
	private double valorVenda;
	private String titulo;
	private String dataPublicada;
	private List<Reportagem> reportagens;

	public Jornal(double ValorVemda, String titulo, String DataPublicada) {
		this.valorVenda = valorVenda;
		this.titulo = titulo;
		this.dataPublicada = dataPublicada;
		this.reportagens = new ArrayList<>();
	}

	public double getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(double valorVenda) {
		valorVenda = valorVenda;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDataPublicada() {
		return dataPublicada;
	}

	public void setDataPublicada(String dataPublicada) {
		dataPublicada = dataPublicada;
	}

	public List<Reportagem> getReportagens() {
		return reportagens;
	}

	public void adicionarReportagem(Reportagem reportagem) {
		reportagens.add(reportagem);
	}

	public void exibirInformacoes() {
		System.out.println("Jornal");
		System.out.println("Título: " + titulo);
		System.out.println("Data de Publicação: " + dataPublicada);
		System.out.println("Valor de Venda: " + valorVenda);
	}

}
