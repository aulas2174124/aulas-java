package Aula4POO2.Exercicio3;

import java.util.ArrayList;
import java.util.List;

//Desenvolva um sistema para uma hamburgueria controlar os pedidos internamente.
//A forma que você irá implementar este sistema será de sua responsabilidade, mas no decorrer do exercício tente explorar vários dos conceitos demonstrados em aula.
//
//Este sistema deve ter obrigatoriamente o cadastro da hamburgueria, clientes, pedidos e produtos fornecidos, mas pode haver outras entidades.
//
//A Hamburgueria lhe encaminhou uma lista de requisitos para o sistema:
//O fluxo padrão: cadastra cliente, monta pedido, seleciona pagamento e retirada/entrega.
//Ao realizar o pedido, o cliente deve ter cadastro básico para contato.
//Limite de 50 pedidos por dia.
//Atualizar o status do pedido manualmente.
//Programar o envio de mensagens aos clientes.
//EXTRA:
//
//Considerando o sistema interno da hamburgueria, desenvolva uma estrutura de acesso para o cliente:
//O cliente pode acessar seu cadastro com email e senha.
//Acompanhar o status do pedido.
//Editar o proprio cadastro.
//Cancelar o pedido.
//Agora a hamburgueria irá precisar de mais requisitos:
//Cadastro de promoções e combos.
//Sistema de pontos de fidelidade.

public class Hamburgueria {
    private String nome;
    private String endereco;
    private String contato;
    private List<Cliente> clientes = new ArrayList<>();
    private List<Pedido> pedidosRealizados = new ArrayList<>();

    public Hamburgueria(String nome, String endereco, String contato) {
        this.nome = nome;
        this.endereco = endereco;
        this.contato = contato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public List<Pedido> getPedidosRealizados() {
        return pedidosRealizados;
    }

    public void adicionarPedido(Pedido pedido) {
        if (pedidosRealizados.size() < 50) {
            pedidosRealizados.add(pedido);
        } else {
            System.out.println("Limite máximo de pedidos diários alcançado.");
        }
    }

    public void adicionarCliente(Cliente cliente) {
        clientes.add(cliente);
    }

    public void listarPedidos() {
        if (pedidosRealizados.isEmpty()) {
            System.out.println("Nenhum pedido encontrado.");
        } else {
            for (Pedido pedido : pedidosRealizados) {
                System.out.println("Número do Pedido: " + pedido.getNumero());
                System.out.println("Cliente: " + pedido.getCliente().toString());
                System.out.println("Itens do Pedido: " + pedido.getItens());
                System.out.println("Status: " + pedido.getStatus());
                System.out.println("Forma de Pagamento: " + pedido.getFormaPagamento());
                System.out.println("Retirada: " + (pedido.isRetirada() ? "Sim" : "Não"));
                System.out.println("Endereço de Entrega: " + pedido.getEnderecoEntrega());
                System.out.println("Taxa de Entrega: " + pedido.getTaxaEntrega());
                System.out.println("----------------------------------------");
            }
        }
    }
}