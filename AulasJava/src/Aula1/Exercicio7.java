package Aula1;

import java.util.Arrays;
import java.util.List;

//7) Usando stream, crie um array com notas de alunos [ [], [], [] ], faça um stream para exibir as médias dos alunos.
//
//obs: se não fizer com o stream, tente fazer de outra maneira.

public class Exercicio7 {
	public static void main(String[] args) {
		 List<String> nomes = Arrays.asList("Aluno 1", "Aluno 2", "Aluno 3");
        double[][] notas = {
                {7.0, 8.8, 6.10},  
                {6.0, 5.0, 8.5},  
                {8.0, 9.0, 10.0}  
        };

        for (int i = 0; i < notas.length; i++) {
            double media = Arrays.stream(notas[i]).average().orElse(0.0);
            System.out.println("A média do " + nomes.get(i) + ": " + media);
    }
	}
}
