package Aula4POO2.Exercicio3;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import Aula4POO2.Exercicio2.Livraria;

public class Main {
	private static Hamburgueria hamburgueria = new Hamburgueria("Hamburgueria", "Do Senhor", "Luís");

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int opcao;
		do {
			exibirMenu();
			opcao = scanner.nextInt();
			scanner.nextLine();

			switch (opcao) {
				case 1:
					cadastrarCliente(hamburgueria, scanner);
					break;
				case 2:
					cadastrarPedido(hamburgueria, scanner);
					break;
				case 3:
					listarPedidos(hamburgueria, scanner);
					break;
				case 4:
					atualizarStatusPedido(scanner);
					break;
				case 0:
					System.out.println("Saindo do sistema...");
					break;
				default:
					System.out.println("Opção inválida.");
			}
		} while (opcao != 0);
	}

	private static void exibirMenu() {
		System.out.println("Menu:");
		System.out.println("1. Cadastrar Cliente");
		System.out.println("2. Realizar Pedido");
		System.out.println("3. Listar Pedidos");
		System.out.println("4. Atualizar Status do Pedido");
		System.out.println("0. Sair");
		System.out.print("Digite a opção desejada: ");
	}

	private static void cadastrarCliente(Hamburgueria hamburgueria, Scanner scanner) {
		System.out.println("Cadastro de Cliente");

		System.out.print("Nome: ");
		String nome = scanner.nextLine();

		System.out.println("Telefone: ");
		String telefone = scanner.nextLine();

		System.out.print("Endereço: ");
		String endereco = scanner.nextLine();

		Cliente cliente = new Cliente();
		cliente.setNome(nome);
		cliente.setTelefone(telefone);
		cliente.setEndereco(endereco);

	}

	private static void cadastrarPedido(Hamburgueria hamburgueria, Scanner scanner) {
		System.out.println("Cadastro de Pedido");

		System.out.print("Nome do cliente: ");
		String cliente = scanner.nextLine();

		System.out.print("Itens do Pedido (separados por vírgula): ");
		String itensInput = scanner.nextLine();
		List<String> itens = Arrays.asList(itensInput.split(","));

		System.out.print("Status do Pedido: ");
		String status = scanner.nextLine();

		System.out.print("Forma de pagamento: ");
		String formaPagamento = scanner.nextLine();

		System.out.print("Retirada (S/N): ");
		String retiradaInput = scanner.nextLine();
		boolean retirada = retiradaInput.equalsIgnoreCase("S");

		System.out.print("Endereço de Entrega: ");
		String endereco = scanner.nextLine();

		System.out.print("Taxa de Entrega: ");
		double taxaEntrega = scanner.nextDouble();
		scanner.nextLine();

		Pedido pedido = new Pedido(cliente, itens, status, formaPagamento, retirada, endereco, taxaEntrega);
		hamburgueria.adicionarPedido(pedido);

		System.out.println("Pedido cadastrado com sucesso!");

	}

	private static void listarPedidos(Hamburgueria hamburgueria, Scanner scanner) {
		hamburgueria.listarPedidos();
	}

	private static void atualizarStatusPedido(Scanner scanner) {
		System.out.println("Atualizar Status do Pedido");

		System.out.print("Número do Pedido: ");
		int numeroPedido;
		try {
			numeroPedido = scanner.nextInt();
			scanner.nextLine();
		} catch (InputMismatchException e) {
			System.out.println("Número do pedido inválido. Certifique-se de digitar um número inteiro.");
			scanner.nextLine();
			return;
		}

		System.out.print("Novo Status: ");
		String novoStatus = scanner.nextLine();

		List<Pedido> pedidos = hamburgueria.getPedidosRealizados();
		boolean pedidoEncontrado = false;

		for (Pedido pedido : pedidos) {
			if (pedido.getNumero() == numeroPedido) {
				pedido.setStatus(novoStatus);
				pedidoEncontrado = true;
				pedido.enviarMensagemCliente();
				break;
			}
		}

		if (pedidoEncontrado) {
			System.out.println("Status do pedido atualizado com sucesso!");
		} else {
			System.out.println("Pedido não encontrado.");
		}
	}

}