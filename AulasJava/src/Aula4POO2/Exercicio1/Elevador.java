package Aula4POO2.Exercicio1;

//Crie uma classe denominada Elevador para armazenar as informações de um elevador dentro de um prédio. 
//A classe deve armazenar o andar atual (térreo = 0), total de andares no prédio (desconsiderando o térreo), capacidade do elevador e quantas pessoas estão presentes nele. A classe deve também disponibilizar os seguintes métodos:
//Inicializa : que deve receber como parâmetros a capacidade do elevador e o total de andares no prédio (os elevadores sempre começam no térreo e vazio); 
//Entra : para acrescentar uma pessoa no elevador (só deve acrescentar se ainda houver espaço); 
//Sai : para remover uma pessoa do elevador (só deve remover se houver alguém dentro dele); 
//Sobe : para subir um andar (não deve subir se já estiver no último andar); 
//Desce : para descer um andar (não deve descer se já estiver no térreo); 
//Encapsular todos os atributos da classe (criar os métodos set e get).

public class Elevador {
	private int andarAtual;
	private int totalAndares;
	private int capacidade;
	private int pessoasPresentes;

	public Elevador(int capacidade, int totalAndares) {
		this.capacidade = capacidade;
		this.totalAndares = totalAndares;
		this.andarAtual = 0;
		this.pessoasPresentes = 0;
	}

	public void entrar() {
		if (pessoasPresentes < capacidade) {
			pessoasPresentes++;
			System.out.println("Uma pessoa acaba de entrar no elevador!");
		} else {
			System.out.println("O elevador esta lotado.");
		}
	}

	public void sair() {
		if (pessoasPresentes > 0) {
			pessoasPresentes--;
			System.out.println("Saiu uma pessoa do elevador!");
		} else
			System.out.println("O elevador esta vazio.");
	}

	public void subir() {
		if (andarAtual < totalAndares) {
			andarAtual++;
			System.out.println("O elevador irá subir para o andar " + andarAtual + "!");
		} else {
			System.out.println("O elevador está no último andar.");
		}

	}

	public void descer() {
		if (andarAtual > 0) {
			andarAtual--;
			System.out.println("O elevador irá descer para o andar " + andarAtual + "!");
		}else
			System.out.println("O elevador está no térreo.");
	}

	public int getAndarAtual() {
		return andarAtual;
	}

	public void setAndarAtual(int andarAtual) {
		this.andarAtual = andarAtual;
	}

	public int getTotalAndares() {
		return totalAndares;
	}

	public void setTotalAndares(int totalAndares) {
		this.totalAndares = totalAndares;
	}

	public int getCapacidade() {
		return capacidade;
	}

	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}

	public int getPessoasPresentes() {
		return pessoasPresentes;
	}

	public void setPessoasPresentes(int pessoasPresentes) {
		this.pessoasPresentes = pessoasPresentes;
	}

}
